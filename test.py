from torch.utils.data import DataLoader
from face_crop import *
from main_functions import *

model = ConvModel()
model.load_state_dict(torch.load('model.pth'))
model.eval()
model.to(device)

fc = FaceCropper()
fc.generate('test7.jpg', show_result=False)
# fc.generate('test2.jpg', show_result=False)
test1 = cv2.imread('image1.jpg', 0)
print(test1.shape)


class TestDataset(Dataset):
    def __init__(self):
        self.X = test1
        self.X = self.X / 255.
        self.X = self.X.astype(np.float32)
        self.X = self.X.reshape(-1, 96, 96)

        self.y = np.zeros_like(self.X)

    def __len__(self):
        return 1

    def __getitem__(self, idx):
        return self.X, self.y


data_test = TestDataset()
loader_test = DataLoader(data_test, batch_size=1)
preds_test = predict(model, loader_test)
plot_face_keypoints(loader_test, is_train=False, preds_test=preds_test)
