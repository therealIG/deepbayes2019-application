from __future__ import division
from data_prepocessing import *
from logger import Logger
import torch
import matplotlib.pyplot as plt
import random
USE_GPU = True
if USE_GPU and torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

print_every = 10

NUM_CLASSES = 30

dtype = torch.float32

model = None
optimizer = None

logger = Logger('./logs')

def RMSE_loss(x, y):
    """
    Average Root Mean Squared Error
    :param x: Scores
    :param y: target
    :return: loss
    """
    mask = y != 0
    mask = mask == 1
    mask = mask.float()
    ground_true = torch.mul(y, mask)
    scores = torch.mul(x,mask)
    n = ground_true.shape[0]
    # x1 = ground_true[:, 0]
    # y1 = ground_true[:, 1]
    # x2 = ground_true[:, 2]
    # y2 = ground_true[:, 3]
    # eye_dist = torch.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
    # eye_dist = eye_dist.view(list(eye_dist.size())[0], 1)
    eye_dist = 1
    loss = torch.div((scores - ground_true) ** 2, eye_dist)
    # loss = torch.mul(loss, mask)
    nonzero_cnt = (loss != 0).sum(dim=1).float()
    nonzero_cnt = nonzero_cnt.view(list(nonzero_cnt.size())[0], 1)
    loss = torch.div(loss, nonzero_cnt)
    loss = torch.sum(loss)
    loss = torch.div(loss, n)
    return loss


def custom_accuracy(x, y):
    """
    Root Mean Squared Error
    :param x: Scores
    :param y: target
    :return: accuracy
    """
    scores = x.clone()
    ground_true = y.clone()
    n = scores.size(0)
    x1 = ground_true[:, 0]
    y1 = ground_true[:, 1]
    x2 = ground_true[:, 2]
    y2 = ground_true[:, 3]
    eye_dist = torch.sqrt((x2 - x1)**2 + (y2 - y1)**2)
    eye_dist = eye_dist.view(list(eye_dist.size())[0], 1)
    acc = torch.div((scores - ground_true)**2, eye_dist)
    acc = torch.sum(acc, dim = 0)
    acc = torch.sum(acc)
    acc = torch.div(acc, n)
    return acc


def plot_face_keypoints(loader, is_train=True, preds_test=[], preds_train=[]):
    data = []
    target = []
    for x, y in loader:
        data.append(x)
        target.append(y)
    x = torch.cat(data, 0)
    y = torch.cat(target, 0)
    x, y = x.numpy(), y.numpy()
    y = (y - 48) / 48
    plt.figure(figsize=(20, 10))
    for i in range(0, 4):
        plt.subplot(1, 4, i + 1)

        rand_img = random.randrange(0, x.shape[0])

        img = x[rand_img]
        img = img.transpose((1, 2, 0))
        img = img.reshape(img.shape[0], -1)
        # img = denormalize(x[rand_img, :, :, :])
        img = np.clip(img, 0, 1.0)
        plt.imshow(img, cmap='gray')

        if is_train:
            cordinates = y[rand_img] * 48 + 48

        else:
            cordinates = preds_test.numpy()[rand_img] * 48 + 48

        if len(preds_train) != 0 :
            plt.scatter(cordinates[::2], cordinates[1::2], marker='o', c='b', s=10)
            cordinates_train = preds_train[rand_img].numpy()
            cordinates_train = cordinates_train * 48 + 48
            plt.scatter(cordinates_train[::2], cordinates_train[1::2], marker='o', c='r', s=10)

        plt.scatter(cordinates[::2], cordinates[1::2], marker='o', c='b', s=10)
        plt.title('Sample n {}'.format(rand_img))
        plt.axis('off')
    plt.show()


def check_accuracy(loader, model):
    if loader.dataset.train:
        print('Checking accuracy on validation set')
    else:
        print('Checking accuracy on test set')
    val_acc = 0.0
    batch_cnt = 0
    model.eval()  # set model to evaluation mode
    with torch.no_grad():
        for x, y in loader:
            x = x.view(-1, 1, 96, 96)
            y = (y - 48) / 48
            x = x.to(device=device, dtype=dtype)  # move to device, e.g. GPU
            y = y.to(device=device, dtype = dtype)
            scores = model(x)
            batch_acc = custom_accuracy(scores, y).item()
            val_acc += batch_acc
            batch_cnt += 1

            img = x.view(-1, 96, 96)[:10].cpu().numpy()

            cordinates = y.cpu().numpy()[:10] * 48 + 48

            pred_cordinates = scores.detach().cpu().numpy()[:10] * 48 + 48

            marked_images = np.empty((10, 370, 370, 4))
            i = 0
            for image, true_cords, pred_cords in zip(img, cordinates, pred_cordinates):
                img = image.reshape(image.shape[0], -1)
                img = np.clip(img, 0, 1.0)
                plt.imshow(img, cmap='gray', interpolation="nearest")
                plt.scatter(true_cords[::2], true_cords[1::2], marker='o', c='b', s=10)
                plt.scatter(pred_cords[::2], pred_cords[1::2], marker='o', c='r', s=10)
                plt.axis('off')
                plt.savefig('img.png', frameon=False)
                tmp = plt.imread('img.png')
                tmp = tmp[57:427, 143:513, :]
                marked_images[i] = tmp
                plt.close()
                i += 1

            # 3. Log training images (image summary)
            info = {'val_landmark': marked_images}

            for tag, images in info.items():
                logger.image_summary(tag, images, 1)

    return val_acc / batch_cnt


def train(model, loader_train, loader_val, optimizer, epochs=1):
    """
    Train a model on CIFAR-10 using the PyTorch Module API.

    Inputs:
    - model: A PyTorch Module giving the model to train.
    - loader_train: DataLoader with train data
    - loader_val: DataLoader with validation data
    - optimizer: An Optimizer object we will use to train the model
    - epochs: (Optional) A Python integer giving the number of epochs to train for

    Returns: Nothing, but prints model accuracies during training.
    """
    model = model.to(device=device)  # move the model parameters to CPU/GPU
    for e in range(epochs):
        for t, (x, y) in enumerate(loader_train):
            flipped_x = x.numpy().copy()
            flipped_x = flipped_x.reshape(x.shape[0], 96, 96)
            flipped_x = flipped_x[:, :, ::-1]
            shift = flipped_x.shape[2]
            shifted_y = y.numpy().copy()
            mask = np.any(np.isnan(shifted_y), axis=1)
            shifted_y = shifted_y[~mask]
            flipped_x = flipped_x[~mask]
            shifted_y[:, ::2] = -1 * (shifted_y[:, ::2] - shift)
            mask = [2, 3, 0, 1, 8, 9, 10, 11, 4, 5, 6, 7, 16, 17, 18, 19, 12, 13, 14, 15, 20, 21, 24, 25, 22, 23, 26,
                    27, 28, 29]

            shifted_y = shifted_y[:, mask]

            augmentation_mask = np.random.choice(flipped_x.shape[0], int(x.shape[0] / 2))

            flipped_x = flipped_x[augmentation_mask]
            shifted_y = shifted_y[augmentation_mask]

            x = x.reshape(-1, 1, 96, 96)
            flipped_x = flipped_x.reshape(-1, 1, 96, 96)
            shifted_y = shifted_y.astype(np.float32)

            tensor_flipped = torch.from_numpy(flipped_x)
            tensor_shifted = torch.from_numpy(shifted_y)

            x = torch.cat((x, tensor_flipped), 0)
            y = torch.cat((y, tensor_shifted), 0)
            y = (y - 48) / 48

            model.train()  # put model to training mode
            x = x.to(device=device, dtype=dtype)  # move to device, e.g. GPU
            y = y.to(device=device, dtype=dtype)

            scores = model(x)
            loss = RMSE_loss(scores, y)

            # Zero out all of the gradients for the variables which the optimizer
            # will update.
            optimizer.zero_grad()

            # This is the backwards pass: compute the gradient of the loss with
            # respect to each  parameter of the model.
            loss.backward()

            # Actually update the parameters of the model using the gradients
            # computed by the backwards pass.
            optimizer.step()

            test_acc = custom_accuracy(scores, y).item()
            if t % print_every == 0:
                print('Iteration %d, loss = %.4f' % (t, loss.item()))
                acc = check_accuracy(loader_val, model)

                # 1. Log scalar values (scalar summary)
                info = {'loss': loss.item(), 'val_dists': acc, 'test_dists': test_acc}

                for tag, value in info.items():
                    logger.scalar_summary(tag, value, e + 1)
                # # 2. Log values and gradients of the parameters (histogram summary)
                # for tag, value in model.named_parameters():
                #     tag = tag.replace('.', '/')
                #     logger.histo_summary(tag, value.data.cpu().numpy(), e + 1)
                #     logger.histo_summary(tag + '/grad', value.grad.data.cpu().numpy(), e + 1)

                img = x.view(-1, 96, 96)[:10].cpu().numpy()

                cordinates = y.cpu().numpy()[:10] * 48 + 48

                pred_cordinates = scores.detach().cpu().numpy()[:10] * 48 + 48

                marked_images = np.empty((10, 370, 370, 4))
                i = 0
                for image, true_cords, pred_cords in zip(img, cordinates, pred_cordinates):
                    # img = image.transpose((1, 2, 0))
                    img = image.reshape(image.shape[0], -1)
                    img = np.clip(img, 0, 1.0)
                    plt.imshow(img, cmap='gray', interpolation="nearest")
                    plt.scatter(true_cords[::2], true_cords[1::2], marker='o', c='b', s=10)
                    plt.scatter(pred_cords[::2], pred_cords[1::2], marker='o', c='r', s=10)
                    plt.axis('off')
                    plt.savefig('img.png', frameon=False)
                    tmp = plt.imread('img.png')
                    tmp = tmp[57:427, 143:513, :]
                    marked_images[i] = tmp
                    plt.close()
                    i += 1
                # 3. Log training images (image summary)
                info = {'landmark': marked_images}

                for tag, images in info.items():
                    logger.image_summary(tag, images, e + 1)


class ConvModel(nn.Module):
    def __init__(self):
        super(ConvModel, self).__init__()
        self.Conv1 = nn.Conv2d(in_channels=1, out_channels=32, kernel_size=4, stride=1, padding=1)
        self.Relu1 = nn.ReLU()
        self.MaxPool1 = nn.MaxPool2d(kernel_size=2, stride=0)

        self.Conv2 = nn.Conv2d(in_channels=32, out_channels=64, kernel_size=3, stride=1, padding=1)
        self.Relu2 = nn.ReLU()
        self.MaxPool2 = nn.MaxPool2d(kernel_size=2, stride=0)

        self.Conv3 = nn.Conv2d(in_channels=64, out_channels=128, kernel_size=2, stride=1, padding=1)
        self.Relu3 = nn.ReLU()
        self.MaxPool3 = nn.MaxPool2d(kernel_size=2, stride=0)

        self.Conv4 = nn.Conv2d(in_channels=128, out_channels=156, kernel_size=1, stride=1, padding=1)
        self.Relu4 = nn.ReLU()
        self.MaxPool4 = nn.MaxPool2d(kernel_size=2, stride=0)

        self.Flat = Flatten()

        self.Linear1 = nn.Linear(7644, 1000)
        self.Relu5 = nn.ReLU()

        self.Linear2 = nn.Linear(1000, 1000)
        self.Relu6 = nn.ReLU()

        self.Linear3 = nn.Linear(1000, 30)

    def forward(self, x):
        x = self.MaxPool1(self.Relu1(self.Conv1(x)))
        x = self.MaxPool2(self.Relu2(self.Conv2(x)))
        x = self.MaxPool3(self.Relu3(self.Conv3(x)))
        x = self.MaxPool4(self.Relu4(self.Conv4(x)))
        x = self.Flat(x)
        x = self.Relu5(self.Linear1(x))
        x = self.Relu6(self.Linear2(x))
        x = self.Linear3(x)

        return x


def predict(model, loader):

    predictions = []
    model.eval()
    with torch.no_grad():
        for x, _ in loader:
            x = x.to(device=device, dtype=dtype)  # move to device, e.g. GPU
            outputs = model(x)
            predictions.append(outputs.data.cpu())
        if len(predictions) > 0:
            return torch.cat(predictions, 0)
