from torch.utils.data import DataLoader
from main_functions import *
import cv2
import torch

cap = cv2.VideoCapture(0)
model = ConvModel()
model.load_state_dict(torch.load('model.pth'))
model.eval()
model.to(device)

CASCADE_PATH = "data/haarcascade_frontalface_alt.xml"


face_cascade = cv2.CascadeClassifier(CASCADE_PATH)

face_crop = None


class TestDataset(Dataset):
    def __init__(self):
        self.X = face_crop
        self.X = self.X / 255.
        self.X = self.X.astype(np.float32)
        self.X = self.X.reshape(-1, 96, 96)

        self.y = np.zeros_like(self.X)

    def __len__(self):
        return 1

    def __getitem__(self, idx):
        return self.X, self.y


while True:
    ret, frame = cap.read()
    img = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(img, 1.1, 3, minSize=(100, 100))
    if faces is None:
        print('Failed to detect face')

    facecnt = len(faces)

    height, width = img.shape[:2]

    nx = 1
    ny = 1
    nr = 1

    for (x, y, w, h) in faces:
        r = max(w, h) / 2
        centerx = x + w / 2
        centery = y + h / 2
        nx = int(centerx - r)
        ny = int(centery - r)
        nr = int(r * 2)

        faceimg = img[ny:ny + nr, nx:nx + nr]
        shape1, shape2 = faceimg.shape
        shape1 -= 96
        shape2 -= 96
        face_crop = cv2.resize(faceimg, (96, 96))
    if face_crop is None:
        continue
    else:
        data_test = TestDataset()
        loader_test = DataLoader(data_test, batch_size=1)
        preds_test = predict(model, loader_test)
        preds_test = preds_test * 48 + 48
        preds_test = preds_test.numpy()[0]
        marked = None
        marked_not_rescaled = None
        for ind in range(0, len(preds_test) - 1, 2):
            marked = cv2.circle(frame, (int(preds_test[ind]+nx + shape1*0.5), int(preds_test[ind+1]+ny+shape2*0.5)),
                                radius=2, color=(0, 255, 0))
            marked_not_rescaled = cv2.circle(face_crop, (int(preds_test[ind]), int(preds_test[ind + 1])),
                                radius=2, color=(0, 255, 0))
        marked = cv2.flip(marked, 1)
        marked_not_rescaled = cv2.flip(marked_not_rescaled, 1)
        cv2.imshow('frame', marked)
        cv2.imshow('frame_2', marked_not_rescaled)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
