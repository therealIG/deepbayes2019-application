from torch.utils.data import sampler
from torch.utils.data import DataLoader
import torch.optim as optim
from main_functions import *
import torch

CONST_FOR_VAL = 642
NUM_TRAIN = FIRST_SIZE - CONST_FOR_VAL
learning_rate = 1e-3
transform = None

batch = 128

data_preprocess('data/training.h5', 'data/training.csv', train=True)

data_train = FacialKeypointsDataset('data/training.h5', transform=transform)

loader_train = DataLoader(data_train, batch_size=batch, sampler=sampler.SubsetRandomSampler(range(NUM_TRAIN)))
loader_val = DataLoader(data_train, batch_size=batch, sampler=sampler.SubsetRandomSampler(range(NUM_TRAIN, NUM_TRAIN+CONST_FOR_VAL)))

print('DATA SUCCESSFULLY LOADED')

model = ConvModel()

optimizer = optim.Adam(model.parameters(), lr=learning_rate, betas=(0.8, 0.999), eps=1e-7)
train(model, loader_train, loader_val, optimizer, epochs=45)

torch.save(model.state_dict(), 'model.pth')
