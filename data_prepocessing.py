import pandas as pd
from torch.utils.data import Dataset
import torch.nn as nn
import numpy as np
import h5py
FIRST_SIZE = 1900
AUGMENTATION_CNT = FIRST_SIZE - 126


def string2image(nparr):
    """Converts a string to a numpy array."""
    string = nparr[0]
    return np.array([int(item) for item in string.split()]).reshape((96, 96))


def data_preprocess(path_to_hdf5, path_to_data, train=True):
    """
    reads csv files and converts it to hdf5 files
    :param path_to_hdf5:
    :param path_to_data:
    :param train: boolean, True if loading training dataset
    :param create: boolean, True if needed to create new hdf5 file
    :return:
    """
    file = h5py.File(path_to_hdf5, 'w')
    fname = path_to_data
    df = pd.read_csv(fname)  # load pandas dataframe
    df = df.dropna()
    df['Image'] = df['Image'].apply(lambda im: np.fromstring(im, sep=' '))
    x = np.vstack(df['Image'].values) / 255.  # scale pixel values to [0, 1]
    x = x.astype(np.float32)
    x = x.reshape(x.shape[0], 96, 96)
    if train:
        y = df[df.columns[:-1]].values
        y = y.astype(np.float32)
    else:
        y = np.zeros((len(x)))
    file.create_dataset('x', data=x)
    file.create_dataset('y', data=y)
    file.close()


class FacialKeypointsDataset(Dataset):
    """
    Facial Keypoints Dataset
    """

    def __init__(self, path, train=True, transform=None):
        """
        Args:
            path (string): Path to the csv file with images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.train = train
        file = h5py.File(path, 'r')
        self.x = file.get('x')
        self.x -= np.mean(self.x) / self.x.shape[0]
        self.y = file.get('y')
        self.transform = transform

    def __len__(self):
        return self.x.shape[0]

    def __getitem__(self, idx):
        x = self.x[idx]
        y = self.y[idx]
        if self.transform:
            x = self.transform(x)

        return x, y


def flatten(x):
    n = x.shape[0]  # read in n, C, H, W
    return x.view(n, -1)


class Flatten(nn.Module):
    def forward(self, x):
        return flatten(x)
